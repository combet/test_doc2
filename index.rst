.. Clumpy documentation master file

Welcome to CLUMPY's documentation!
==================================================

Contents:

.. toctree::
   :maxdepth: 2

.. role:: math(raw)
   :format: html latex
..

::

    I.  WHAT IS CLUMPY? it's great !
    II. INSTALLATION AND COMPILATION
    III. FITS VIEWERS (FOR 2D SKYMAPS)

--------------

What is clumpy?
==================

We hope you will enjoy using CLUMPY whether you are:

* *an experimentalist* looking for synthetic γ-ray and/or ν skymaps to calculate your instrument sensitivity or to use in model/template analyses;
* *an astrophysicist* working on the DM content of dSphs and wishing to perform a Jeans analysis on his kinematic data and/or calculate its J factor and expected fluxes;
* *a theoretician* wishing to plug his/her preferred particle physics model and see what is the corresponding γ-ray or ν flux in the Galaxy, dSphs,... for decay and/or annihilation.

Physics goals
--------------------------------------------------------------------------
CLUMPY addresses the following physics problems of dark matter annihilation/decay signals for all targets in the sky:

* astrophysical J-factors from spherical or triaxial DM distribution in the Galaxy (smooth, mean and/or drawn sub-haloes) and/or for nearby haloes;
* γ-ray and ν fluxes for galactic and/or extragalactic contributions (combining the astrophysical factor and the particle physics factor);

as well as performing Jeans analyses to reconstruct DM profiles from kinematic data (χ2 analysis or MCMC analysis) and calculate the associated J-factors.

Ingredients
--------------------------------------------------------------------------
J-factor and flux calculations require many inputs, as detailed in the code documentation and in `CLUMPY publications <http://adsabs.harvard.edu/abs/2012CoPhC.183..656C,2016CoPhC.200..336B>`__. These inputs are handled by parametrisations (and keywords), e.g., for:

* Haloes and sub-haloes:  profile parametrisations (ZHao, NFW, Einasto, etc.), concentration-mass dependence (Bullock, Moliné, etc.),…;
* Extragalactic contribution: cosmology parameters, mass distribution (Press-Schecter, Tinker, etc.), absorption (Finke, Franceschini, etc.), …;
* Jeans analysis: light profile (Plummer, Zhao, etc.), anisotropy profile (constant, Osipkov, etc.), …;
* Skymaps: integration angle, smoothing (by Gaussian PSF), angular power spectrum (APS), ….

For more information and documentation on the code, please visit
lpsc.in2p3.fr/clumpy/.

Language and third-party softwares
--------------------------------------------------------------------------
The CLUMPY package is developped in C/C++ (but no classes) and is interfaced with:
  
* `ROOT CERN library <https://root.cern.ch/>`__ (for displays);
* `CFITSIO <http://heasarc.gsfc.nasa.gov/fitsio/>`__ for (FITS outputs);
* `HEALPix <http://sourceforge.net/projects/healpix>`__ (skymap pixelization scheme and APS);
* `GSL <http://www.gnu.org/software/gsl>`__ (Gnu scientific library);

and two optionnal packages

* `Doxygen <http://www.stack.nl/~dimitri/doxygen/>`__ (to generate the documentation) and
* `GreAT <http://lpsc.in2p3.fr/great>`__ (for MCMC Jeans analyses). See below for more information and troubleshooting installing these softwares.

History (of development)
--------------------------------------------------------------------------

* V1 or V11.09 [2011/09] -- *A code for γ-ray signals from dark matter structures*; `Charbonnier, Combet, & Maurin, CPC 183, 656 (2012) <http://cdsads.u-strasbg.fr/abs//2012CoPhC.183..656C>`__.
* V2 or V15.06 [2015/06] -- *Jeans analysis, γ-ray and ν fluxes from dark matter (sub-)structures;* `Bonnivard et al., CPC 200, 336 (2016) <http://cdsads.u-strasbg.fr/abs/2016CoPhC.200..336B>`__.
* V3 or V17.?? [2017/??] -- *Extragalactic γ-ray and ν fluxes from dark matter;* Hütten, Combet, & Maurin (in prep.).

CLUMPY crew/contacts
--------------------------------------------------------------------------

* Vincent Bonnivard (*Jeans analysis*)
* Aldée Charbonnier (*CLUMPY pre-version*)
* `Celine Combet <celine.combet@lpsc.in2p3.fr>`__ (*dark matter distributions, clump drawing, extragalactic*)
* `Moritz Hütten <moritz.huetten@desy.de>`__ (*HEALPix/FITS implementation, APS, extragalactic*)
* `David Maurin <dmaurin@lpsc.in2p3.fr>`__ (*project coordinator*)
* `Emmanuel Nezri <Emmanuel.Nezri@lam.fr>`__ (*particle physics implementation*).


Installation and compilation
=============================
  
Before focusing on CLUMPY installation (end of this section), we go briefly through the installation of its dependences.

*N.B.: for MACOS, installing ROOT and CFISTIO via`homebrew <http://brew.sh/>`__ is probably the best way.*

* `GSL (Gnu scientific library) <http://www.gnu.org/software/gsl>`__: directly install the package for your distribution (or, if needed, install it locally). Do not forget to install the development files gsl-devel to also get the headers files to include!
* `CFITSIO <http://heasarc.gsfc.nasa.gov/fitsio/>`__: directly install the package for your distribution (or, if it does not exist for your system, `download it <http://heasarc.gsfc.nasa.gov/fitsio/>`__ and install it).
* `ROOT <http://root.cern.ch/drupal>`__: You may retrieve a pre-compiled binary version of ROOT (e.g., using , but compaling it may solve some later difficulties. Be aware that installing properly ROOT is probably the most complicated step to have a CLUMPY version running (ROOT is huge, but you usually have a lot of support in ROOT forums).
        - Visit http://root.cern.ch/
        - Download your preferred ROOT version (e.g., 5.34/36 or the latest version 6)
	- Install ROOT (see details on ROOT website)
	      + Start with some prerequisite for your system (https://root.cern.ch/build-prerequisites)
	      + Configure and compile, enabling *minuit2*: it usually goes as  (see details in ROOT README) : for version <=5, when configuring, use ``./configure --enable-minuit2 ...`` ; for version 6, before the built, use *ccmake* to modify the configuration (*ccmake* tool) and enable *minuit2*

5. `GreAT <http://lpsc.in2p3.fr/great>`__ [*optional*]: if you want to
   run MCMC/Jeans analyses, follow the download/installation instruction
   from GreAT `here <http://lpsc.in2p3.fr/great>`__ or directally from
   `GreAT on Gitlab <https://gitlab.in2p3.fr/derome/GreAT>`__.
6. `Doxygen <http://www.stack.nl/~dimitri/doxygen/>`__ [*optional*]: if
   you wish to re-generate CLUMPY documentation locally (rather than
   browsing it `online <http://lpsc.in2p3.fr/clumpy>`__, you need to
   instal doxygen.

Before going to the next step and compiling CLUMPY, make sure that all
the environmental variables for CLUMPY and its dependences are correctly
set in your ``.bashenv /.zshenv`` file (or alternatively, set the paths
directly in the CLUMPY Makefile). It should look like:

::

        ## GSL
        export GSL=path_to_your_GSL_installation
        export PATH=$PATH:$GSL
        export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$GSL/.libs
        ## ROOT
        export ROOTSYS=path_to_your_ROOT_installation
        export ROOTLIBS=$ROOTSYS/lib
        export PATH=$PATH:$ROOTSYS/bin
        export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ROOTSYS/lib
        ## CFITSIO and HEALPix
        export FITSIOLIBS_PATH=path_to_your_cfitsio_lib
        export FITSIOINCLUDE_PATH=path_to_your_cfitsio_include
        export HEALPIXCPPLIBS_PATH=path_to_your_healpixc++_lib
        export HEALPIXCPPINCLUDE_PATH=path_to_your_healpixc++_include
        export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HEALPIXCPPLIBS_PATH
        ## GREAT
        export GREAT=path_to_your_great_installation
        export PATH="${PATH}":"${GREAT}/bin"
        export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${GREAT}/lib

| **CLUMPY installation and compilation** 1. *Clone* from git
| ``> git clone git@gitlab.in2p3.fr:david-maurin/CLUMPY.git`` 2. Set
  \*\_\ :math:`CLUMPY_ environment variable*: add in your `~/.bashrc` file  ```  > # CLUMPY environment (for bash shell below)  > export CLUMPY=absolute_path_to_CLUMPY_dir  > export PATH="`\ {PATH}":":math:`{CLUMPY}/bin"  > export LD_LIBRARY_PATH=`\ LD\_LIBRARY\_PATH:\ :math:`CLUMPY/lib  > # For MAC-OS, also add the following line  > export DYLD_LIBRARY_PATH=`\ DYLD\_LIBRARY\_PATH:$CLUMPY/lib
| ``3. *Compile* (use N cores to speed up the compilation)`` > cd
  $CLUMPY
| > make -jN (N=number of cores in your system)
  ``4. *Re-generate documentation* (optional)`` > doxygen Doxyfile
  ``and then open with your preferred browser the file``\ html/index.html\`\`\`

**CLUMPY execution**

::

        > ./bin/clumpy       [to access all CLUMPY options]
        > ./bin/jeansChi2    [for Jeans analysis]
        > ./bin/jeansMCMC    [if GreAT is installed]

Run examples are provided at
http://lpsc.in2p3.fr/clumpy/quick_checks.html so that you can check the
various options available and the expected outputs.

**Tested to compile and run on**

    System Root compiler (C++/Fortran)

-  Scientific Linux 6.6 5.34.14 gcc 4.4.7/gcc 4.4.7 (Moritz)
-  Ubuntu 14.10 (David)
-  Ubuntu 12.04 LTS 5.34.17 gcc 4.6.4/gcc 4.6.4 (Moritz)
-  Fedora 25 (need to check what gcc, Céline)
-  Mageia 4 5.34.18 g++ 4.8.2 (Celine)
-  Mageia 4 5.34.30 g++ 4.8.2 (David)
-  Mageia 4 6.04.00 g++ 4.8.2 (Celine)
-  Mac OSX 10.9.5 5.34.20 clang-600.0.56/gfortran 4.9.0 (Moritz)
-  Mac OSX 10.8.5 5.34.10 clang-500.2.75 (Vincent)
-  Mac OSX 10.7 5.34.03 clang-318.0.61 (Celine)

   -  Main parameter file (loaded for all clumpy run):
      clumpy\_params.txt => user-defined CLUMPY parameters Other input
      files : Related to the J calculation of a list DM haloes: -
      data/list\_generic.txt => list of spherical DM haloes -
      data/list\_generic\_triaxial.txt => list of triaxial DM haloes
      Related to ./clumpy -s option : - data/stat\_example.dat =>
      example file of allowed parameters on a single DM halo Related to
      Jeans analysis : - data/list\_generic\_jeans.txt => list of DM
      haloes for computing Jeans-related quantities -
      data/params\_jeans.txt => user-defined parameters for a Jeans
      analysis - data\_light.txt => sample of simulated surface
      brightness data (for a dSph) - data\_sigmap.txt => sample of
      simulated velocity dispersion data (for a dSph) - data\_vel.txt =>
      sample of simulated line-of-sight velocities (for a dSph)

Content
--------------------------------------------------------------------------


Directories:

::

    bin/               => all exec files (generated by 'make')
    data/              => CLUMPY parameter file + dSph definition files (user-defined)
    DocImages/         => contains images used by the documentation
    html/              => html documentation (generated by 'doxygen')
    include/           => .h files
    latex/             => latex documentation (generated by 'doxygen')
    lib/               => library 'libclumpy.a' for this package (generated by 'make')
    obj/               => .o files (generated by 'make')
    output/            => default directory for CLUMPY runs (empty after installation)
    src/               => .cc files 
    data/PPPC4DMID-spectra/ => contains PPPC4DMID files for DM spectra

Files:

::

    Doxyfile   => doxygen parameter file ('doxygen Doxyfile')
    Makefile   => rules to compile
    README     => this file

    If everything has been correctly set, you should now be able to
    successfully compile CLUMPY!

    If you have a Python installation, you may also want to install
    Healpy for plotting and post-processing the 2D-skymaps via Python. A
    minimal example script how to load the Clumpy output into Python is
    part of the Clumpy package. Adapt it and execute it via:

    ::

        python plotClumpyMaps.py

    A nice tutorial into Healpy can be found at
    http://healpy.readthedocs.org.

Special remarks on Cfitsio, HEALPix and Clumpy compilation on Mac OSX
--------------------------------------------------------------------------
-  On Mac OSX 10.7/10.8/10.9, it should be possible to install both
   cfitsio and the Healpix Fortran and C++ packages the same way as
   described above for a Linux environment. In case you have not
   installed any Fortran compiler, we recommend gfortran, to download
   from https://gcc.gnu.org/wiki/GFortranBinaries

   The installation then also contains the gfortran libraries needed for
   a successful compilation of Clumpy.

-  If the cfitsio installation does not work this way, we recommend
   installing it via homebrew http://brew.sh/ (probably also the nicest
   way for installing ROOT on MacOS), with:

   ::

       brew install cfitsio

   Homebrew puts the installation usually into:

   ::

       /usr/local/Cellar/cfitsio

-  For then compiling Clumpy, you may specify within the Makefile the
   location of the gfortran libraries (or add them to the library path)
   at FORTRANLIBS\_PATH\_LINK
-  If the compilation fails because -lgomp or -lgfortran cannot be
   found:

   -  also choose (same as on Linux) the correct compilers for C++
      (default Clang++) and Fortran (e.g.,
      /usr/local/gfortran/bin/gfortran).
   -  everything else done differently in a Mac environment should be
      already accounted for in the Makefile.

GreAT installation instructions
--------------------------------------------------------------------------


The GreAT package can be downloaded from http://lpsc.in2p3.fr/great.

-  Installation

Please type:

::

    git clone https://gitlab.in2p3.fr/derome/GreAT.git
    cd GreAT
    cmake .
    make

Libraries will be installed in GreAT/lib. Executables will be installed
in GreAT/bin.

**Troubleshooting** - Some problems can occur at the execution of the
code with Mac OSX 10.8.5 if using a ROOT version more recent than
5.34.10 [e.g., issues with TF3 with option -g7]. Please use ROOT 5.34.10
or try to update your OS. - Compilation fails with "\`g++\`: argument to
-I missing": Have you set and sourced the include paths
FITSIOINCLUDE\_PATH and HEALPIXCPPINCLUDE\_PATH correctly? - HEALPix C++
libraries and includes have disappeared: If you leave your installation
in the src/cxx/generic\_gcc (or corresponding) folders, a make clean
will delete your installation again. Unfortunately, to avoid this issue,
you have to copy the installation by hand to another folder. - HEALPix
installation with the standard installation procedure on MacOSX: some
versions of the Clang compiler do not accept the flag "-fno-tree-fre"
which is required for installing Healpix. Try to remove it from
LS\_OPTFLAGS in src/cxx/config/config.osx. The compiler might mention a
missing library called lgomp. This library can be found in the gcc
directory. Try to add -L/PATH\_TO\_YOUR\_GCC/ to CFITSIO\_EXT\_LIB in
the Healpix Makefile.

-  HEALPix installation: If both the above described ways to install
   HEALPix fail, try the following. Go to: cd src/cxx/autotools/ in your
   Healpix source folder, then type: autoreconf --install ./configure
   -prefix=:math:`HEALPIXDIR LDFLAGS=-L`\ FITSIOLIBS\_PATH
   CPPFLAGS=-I$FITSIOINCLUDE\_PATH

   where $HEALPIXDIR stands for the directory you want to install the
   C++ lib/ and include/ folders to, and
   $FITSIOLIBS\_PATH/$FITSIOINCLUDE\_PATH should point to the
   library/include files of your CFITSIO installation. Then type same
   folder:

   ::

       make install

   This will install a shared and static library in the $HEALPIXDIR/lib
   folder, and the include files in $HEALPIXDIR/include/healpix\_cxx.

   However, you will have to modify the Makefile now. Please replace the
   line:

   ::

       HEALPIXCPPLIBS    = -L$(HEALPIXCPPLIBS_PATH) -lgomp -lhealpix_cxx -lsharp -lc_utils -lcxxsupport -lfftpack 

   by:

   ::

       HEALPIXCPPLIBS    = -L$(HEALPIXCPPLIBS_PATH) -lgomp -lhealpix_cxx

   When compiling against the shared library, you also have to add the
   $HEALPIXDIR/lib to your LD\_LIBRARY\_PATH.



Fits viewers for 2D skymaps
==============================

ASCII outputs and `FITS <https://en.wikipedia.org/wiki/FITS%5D>`__
skymaps (if applies) are provided.

| The following viewers are supported to display the 2D-skymap
  fits-files:
| - `ds9 <http://ds9.si.edu>`__, supports Healpix maps from version 7
  on. ds9 is able to display the post-processed (-o2 option) fits-files.
| To get the right colour scale for cut-sky maps, go to Scale -> Scale
  Parameters (at the very bottom) and raise the lower limit to
| get an appropriate contrast. You can switch the coordinate system as
  well to Galactic coordinates and display horizontal and vertical
  1D-profiles. - `fv (fits
  viewer) <http://heasarc.gsfc.nasa.gov/ftools/fv/>`__ Only to browse
  the headers? - `Aladin <http://aladin.u-strasbg.fr>`__ Output and
  post-processed (-o2 option) fits-files, but only for NSIDE = 2^n. -
  *Healpy* (part of the Healpix package) Is able to display the
  post-processed (-o2 option) fits-files, but only for NSIDE = 2^n. -
  `Lambda
  Skyviewer <http://lambda.gsfc.nasa.gov/toolbox/tb_skyviewer_ov.cfm>`__


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

Fork this project
==================

* https://gitlab.com/pages/sphinx
